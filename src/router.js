import Vue from 'vue'
import VueRouter from 'vue-router'
import Animal from './views/Animal.vue'
import Situation from './views/Situation.vue'


Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Animal',
    component: Animal
  },
  {
    path: '/Situation',
    name: 'Situation',
    component: Situation
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router

