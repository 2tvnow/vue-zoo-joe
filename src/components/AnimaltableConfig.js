export default {
    gender: ['男生', '女生'],
    species:['哺乳類','鳥類','魚類','昆蟲','兩棲類'],
    health: ['虛弱','傷殘','健康','普通','待檢查'],
    dialog: false,
    headers: [
        {
          text: '動物姓名',
          align: 'start',
          sortable: false,
          value: 'name',
        },
        { text: '物種', value: 'species' },
        { text: '性別', value: 'gender' },
        { text: '年齡', value: 'age' },
        { text: '健康狀況', value: 'health' },
        { text: '編輯', value: 'actions', sortable: false },
      ]
}