import Vue from 'vue'
import App from './App.vue'
import router from './router.js'
import store from './store.js'
import vuetify from './vuetify.js'
import ApexCharts from 'vue-apexcharts';

Vue.config.productionTip = false

Vue.component('apexchart', ApexCharts)
new Vue({
  router,
  store,
  vuetify,
  render: h => h(App)
}).$mount('#app')
