import Vue from 'vue'
import Vuex from 'vuex'
import originalDataSet from './resourses/animalsDataList.json'
Vue.use(Vuex)


export default new Vuex.Store({
  state: {
    animal: []
  },
  mutations: {
    ADD: function (state, item) {
      state.animal.push(item)
    },

    EDIT: function (state, item) {
      Object.assign(state.animal[item.index], item.item)
    },

    DELETE: function (state, index) {
      state.animal.splice(index, 1)
    },

    REMOVE_ALL_ITEMS :function () {
        this.state.animal = []
    },
    INITIALIZE :function () {
        originalDataSet.map((item) => {
          this.state.animal.push({
              name: item.name,
              species: item.species,
              gender: item.gender,
              age:item.age,
              health:item.health
          })
        })
    }//end of generateDataset();
  },
  actions: {
    async initialize(context){
      await context.commit('REMOVE_ALL_ITEMS')
      await context.commit('INITIALIZE')
    },
    add: function ({ commit }, item) {
      commit('ADD', item)
    },
    edit: function ({ commit }, item) {
      commit('EDIT', item)
    },
    delete: function ({ commit }, index) {
      commit('DELETE', index)
    }
  },
  getters: {
    getAnimaltable: function (state) {
      return state.animal
    }
  }
})

